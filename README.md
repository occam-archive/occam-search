# Occam Search Component

This optional plugin adds the `search` component and its associated commands
to Occam. These add more rigorous search functionality to compatible Occam
clients.

To add new plugins, you would need to provide a plugin implementation based on
the `SearchIndexPlugin` base class and place it within the
`occam.search.plugins` import path.

## Plugin Interface

Each plugin must provide several functions. You can inspect the functions
provided by the base class `SearchIndexPlugin`. You add a new search
interface by extending this base class and providing the appropriate functions.

### `detect`

This function will return `True` if the search index is available and installed
on the system. The point of this function is to rule out plugins that cannot be
used. For instance, if the underlying daemon or commands are not installed on
the system.

### `initialize`

This function will perform the one-time setup of the index. It is up to the
search index software to perform whatever it needs to such that it is ready to
record any subsequent records via `set` and return results via `fullSearch`.

### `clear`

This function clears all entries in the index.

### `fullSearch`

This function performs a search and returns a dict containing metadata
and object data representing the search results. The criteria for the search is
provided by the arguments to the function.

### `set`

This function adds a new entry into the search index. The function is given the
object metadata. It is up to the search index to record that information in any
way it chooses in order to return valid results in a subsequent `fullSearch`
invocation.
