# Elasticsearch Search Plugin

This plugin implements [Elasticsearch](https://www.elastic.co/elasticsearch/)
indexing.

## Configuration

In your Occam `config.yml`, you can configure the Elasticsearch endpoint within
the `search`/`plugins`/`elasticsearch` section, as such:

```
search:
  plugins:
    elasticsearch:
      host: localhost
      port: 9200
      prefix: occam
```

The above are the defaults if the configuration is not provided. These defaults
will connect to a local Elasticsearch node using the default port of `9200`. If
you are hosting your Elasticsearch server on a different machine, or serving it
using a different port, you must change these values in the configuration.

The `prefix` option sets the index name prefix. Each index created for Occam
will be given a name. If you have multiple Occam nodes using the same
Elasticsearch server, you might need to change the prefix. Note: changing the
prefix will essentially detatch a populated index. That is, it will not see
the old index and will be empty.
