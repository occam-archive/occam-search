# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.manager          import uses

from occam.commands.manager import command, option

from occam.search.manager   import SearchManager
from occam.objects.manager  import ObjectManager

@command('search', 'build',
         category      = "Search",
         documentation = "Builds the search index from the existing object store.")
@uses(ObjectManager)
@uses(SearchManager)
class SearchBuildCommand:
  def do(self):
    """ This command aggressively populates the current index.
    """

    return 1
