# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.log              import Log
from occam.manager          import uses

from occam.commands.manager import command, option

from occam.search.manager   import SearchManager

@command('search', 'get',
  category      = "Search",
  documentation = "Look up object details only from our local knowledge.")
@option("-q", "--query",        action  = "store",
                                dest    = "query",
                                help    = "search this text in all fields")
@option("-t", "--type",         action  = "store",
                                dest    = "object_type",
                                help    = "search for objects with the specified type")
@option("-s", "--subtype",      action  = "append",
                                dest    = "subtype",
                                nargs   = "?",
                                help    = "search for objects with the specified subtype")
@option("-n", "--name",         action  = "store",
                                dest    = "name",
                                help    = "search for objects with the specified name")
@option("-i", "--id",           action  = "store",
                                dest    = "id",
                                help    = "search for the object with the specified id")
@option("-u", "--uid",          action  = "store",
                                dest    = "uid",
                                help    = "search for the object with the specified uid")
@option("-g", "--tag",          action  = "append",
                                dest    = "tags",
                                nargs   = "?",
                                help    = "search for objects with these tags")
@option("-e", "--environment",  action  = "append",
                                dest    = "environments",
                                type    = str,
                                help    = "search for objects with the specified environment")
@option("-a", "--architecture", action  = "append",
                                dest    = "architectures",
                                type    = str,
                                help    = "search for objects with the specified architecture")
@option("--excludeEnvironment", dest    = "excludeEnvironments",
                                action  = "append",
                                type    = str,
                                help    = "filter out objects by the given environment")
@option("--excludeArchitecture", dest    = "excludeArchitectures",
                                action  = "append",
                                type    = str,
                                help    = "filter out objects by the given architecture")
@option("--environments",       action  = "store",
                                dest    = "searchEnvironments",
                                help    = "search for known environments")
@option("--architectures",      action  = "store",
                                dest    = "searchArchitectures",
                                help    = "search for known architectures")
@option("--types",              dest    = "return_types",
                                action  = "store",
                                type    = str,
                                default = None,
                                nargs   = "?",
                                help    = "search types matching the given string")
@option("-x", "--excludeType",  dest    = "excludeTypes",
                                action  = "append",
                                type    = str,
                                help    = "filter objects by their type")
@option("-v", "--views",        action  = "store",
                                dest    = "views",
                                nargs   = "+",
                                help    = "search for objects that can view the specified type. a subtype may follow.")
@option("-p", "--provides",     action  = "store",
                                dest    = "provides",
                                nargs   = 2,
                                help    = "search of objects that provide the given environment and architecture")
@option("-d", "--identity",     action  = "store",
                                dest    = "identity",
                                type    = str,
                                help    = "search for objects signed by the given identity.")
@option("-f", "--full",         dest    = "full",
                                action  = "store_true",
                                help    = "returns full aggregate information about the search")
@option("-j", "--json",         dest    = "to_json",
                                action  = "store_true",
                                help    = "returns result as a json document")
@uses(SearchManager)
class SearchGetCommand:
  def do(self):
    """ Performs a full search.
    """

    # Determine the identity of the search actor
    identity = ""
    if self.person:
      identity = self.person.identity
      if hasattr(self.person, "roles") and 'administrator' in self.person.roles:
        identity = None

    # Perform the search
    if not self.options.return_types is None:
      Log.header(key="occam.objects.commands.search.listingTypes")

      types = self.objects.searchTypes(query = self.options.return_types)

      if self.options.to_json:
        Log.output(json.dumps(types))
      else:
        for object_type in types:
          Log.output(object_type)
    elif not self.options.searchEnvironments is None:
      Log.header(key="occam.objects.commands.search.listingEnvironments")

      rows = self.objects.searchEnvironments(query = self.options.searchEnvironments)

      if self.options.to_json:
        Log.output(json.dumps(rows))
      else:
        for result in rows:
          Log.output(result)
    elif not self.options.searchArchitectures is None:
      Log.header(key="occam.objects.commands.search.listingArchitectures")

      rows = self.objects.searchArchitectures(query = self.options.searchArchitectures)

      if self.options.to_json:
        Log.output(json.dumps(rows))
      else:
        for result in rows:
          Log.output(result)
    else:
      ret = self.search.fullSearch(query = self.options.query,
                                   id = self.options.id,
                                   uid = self.options.uid,
                                   name = self.options.name,
                                   object_type = self.options.object_type,
                                   environments = self.options.environments,
                                   architectures = self.options.architectures,
                                   tags = self.options.tags or [],
                                   subtype = self.options.subtype or [],
                                   excludeEnvironments = self.options.excludeEnvironments or [],
                                   excludeArchitectures = self.options.excludeArchitectures or [],
                                   excludeTypes = self.options.excludeTypes or [],
                                   canBeReadBy = identity)

      Log.header(key="occam.objects.commands.search.header")

      if self.options.to_json:
        ret['objects'] = [
          {
            "uid":          obj.uid,
            "id":           obj.id,
            "revision":     obj.revision,
            "name":         obj.name,
            "owner": {
              "id":         obj.owner_id or obj.id
            },
            "identity":     obj.identity_uri,
            "summary":      obj.description,
            "type":         obj.object_type,
            "subtype":      list(obj.subtype or []),
            "architecture": obj.architecture,
            "organization": obj.organization,
            "environment":  obj.environment,
            "images":       list(obj.images or []),
            "score":        obj._data.get('score', 0) # Hidden field
          } for obj in ret['objects']
        ]

        ret['numTypes'] = ret['num_types']
        del ret['num_types']
        ret['numEnvironments'] = ret['num_environments']
        del ret['num_environments']
        ret['numArchitectures'] = ret['num_architectures']
        del ret['num_architectures']
        ret['numTags'] = ret['num_tags']
        del ret['num_tags']

        if self.options.full:
          Log.output(json.dumps(ret))
        else:
          Log.output(json.dumps(ret['objects']))
      else:
        for obj in ret['objects']:
          Log.output((obj.object_type or "object") + " " + (obj.name or "unknown"))
          Log.output(obj.id,  padding="      id: ")

      Log.write(key="occam.objects.commands.search.found", count = ret['count'])

    return 0 
