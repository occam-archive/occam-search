# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.manager          import uses

from occam.commands.manager import command, option

from occam.search.manager   import SearchManager

@command('search', 'clear',
         category      = "Search",
         documentation = "Clear the search index.")
@uses(SearchManager)
class SearchClearCommand:
  def do(self):
    """ This command clears the current index.
    """

    if self.search.clear():
      return 0

    return 1
